(function() {
  var _bookie;

  _bookie = _bookie || [];

  (function() {
    var g, h, j, results;
    h = (_bookie["slice"] ? _bookie.slice(0) : []);
    _bookie = {
      track_referrer: true,
      image: new Image(),
      track: function() {
        var a, b, c, d, f, i;
        this.setCookie("_bookie_cookie", 1, 1);
        a = this.url();
        if (a) {
          this.image.src = a;
          b = 60 * 60;
          d = b * 24;
          f = d * 31;
          c = d * 365;
          i = c * 10;
          if (!this.getCookie("_bookie_unique_hour")) {
            this.setCookie("_bookie_unique_hour", 1, b);
          }
          if (!this.getCookie("_bookie_unique_day")) {
            this.setCookie("_bookie_unique_day", 1, d);
          }
          if (!this.getCookie("_bookie_unique_month")) {
            this.setCookie("_bookie_unique_month", 1, f);
          }
          if (!this.getCookie("_bookie_unique_year")) {
            this.setCookie("_bookie_unique_year", 1, c);
          }
          if (!this.getCookie("_bookie_unique")) {
            return this.setCookie("_bookie_unique", 1, i);
          }
        }
      },
      push: function(a) {
        var b;
        b = a.shift();
        if (b === "track") {
          return _bookie.track();
        }
      },
      url: function() {
        var a, b, d;
        a = void 0;
        b = void 0;
        d = this.$("bookie-tracker");
        if (d) {
          b = d.getAttribute("data-bookie-id");
          a = d.src.replace("/bookie.js", "/track.gif");
          a += "?h[site_id]=" + b;
          a += "&h[resource]=" + this.resource();
          a += "&h[referrer]=" + this.referrer();
          a += "&h[title]=" + this.title();
          a += "&h[user_agent]=" + this.agent();
          a += "&h[unique]=" + this.unique();
          a += "&h[unique_hour]=" + this.uniqueHour();
          a += "&h[unique_day]=" + this.uniqueDay();
          a += "&h[unique_month]=" + this.uniqueMonth();
          a += "&h[unique_year]=" + this.uniqueYear();
          a += "&h[screenx]=" + this.screenWidth();
          a += "&h[browserx]=" + this.browserWidth();
          a += "&h[browsery]=" + this.browserHeight();
          a += "&timestamp=" + this.timestamp();
        }
        return a;
      },
      domain: function() {
        return window.location.hostname;
      },
      referrer: function() {
        var a, e1, e2, error, error1;
        a = "";
        if (!this.track_referrer) {
          return a;
        }
        this.track_referrer = false;
        try {
          a = top.document.referrer;
        } catch (error) {
          e1 = error;
          try {
            a = parent.document.referrer;
          } catch (error1) {
            e2 = error1;
            a = "";
          }
        }
        if (a === "") {
          a = document.referrer;
        }
        return this.escape(a);
      },
      agent: function() {
        return this.escape(navigator.userAgent);
      },
      escape: function(a) {
        if (typeof encodeURIComponent === "function") {
          return encodeURIComponent(a);
        } else {
          return escape(a);
        }
      },
      resource: function() {
        return this.escape(document.location.href);
      },
      timestamp: function() {
        return new Date().getTime();
      },
      title: function() {
        if (document.title && document.title !== "") {
          return this.escape(document.title);
        } else {
          return "";
        }
      },
      uniqueHour: function() {
        if (!this.getCookie("_bookie_cookie")) {
          return 0;
        }
        if (this.getCookie("_bookie_unique_hour")) {
          return 0;
        } else {
          return 1;
        }
      },
      uniqueDay: function() {
        if (!this.getCookie("_bookie_cookie")) {
          return 0;
        }
        if (this.getCookie("_bookie_unique_day")) {
          return 0;
        } else {
          return 1;
        }
      },
      uniqueMonth: function() {
        if (!this.getCookie("_bookie_cookie")) {
          return 0;
        }
        if (this.getCookie("_bookie_unique_month")) {
          return 0;
        } else {
          return 1;
        }
      },
      uniqueYear: function() {
        if (!this.getCookie("_bookie_cookie")) {
          return 0;
        }
        if (this.getCookie("_bookie_unique_year")) {
          return 0;
        } else {
          return 1;
        }
      },
      unique: function() {
        if (!this.getCookie("_bookie_cookie")) {
          return 0;
        }
        if (this.getCookie("_bookie_unique")) {
          return 0;
        } else {
          return 1;
        }
      },
      screenWidth: function() {
        var e, error;
        try {
          return screen.width;
        } catch (error) {
          e = error;
          return 0;
        }
      },
      browserDimensions: function() {
        var a, b;
        a = 0;
        b = 0;
        try {
          if (typeof window.innerWidth === "number") {
            a = window.innerWidth;
            b = window.innerHeight;
          } else if (document.documentElement && document.documentElement.clientWidth) {
            a = document.documentElement.clientWidth;
            b = document.documentElement.clientHeight;
          } else if (document.body && document.body.clientWidth) {
            a = document.body.clientWidth;
            b = document.body.clientHeight;
          }
        } catch (undefined) {}
        return {
          width: a,
          height: b
        };
      },
      browserWidth: function() {
        return this.browserDimensions().width;
      },
      browserHeight: function() {
        return this.browserDimensions().height;
      },
      $: function(a) {
        if (document.getElementById) {
          return document.getElementById(a);
        }
        return null;
      },
      setCookie: function(a, b, d) {
        var c, f;
        f = void 0;
        c = void 0;
        b = escape(b);
        if (d) {
          f = new Date();
          f.setTime(f.getTime() + (d * 1000));
          c = "; expires=" + f.toGMTString();
        } else {
          c = "";
        }
        return document.cookie = a + "=" + b + c + "; path=/";
      },
      getCookie: function(a) {
        var b, c, d, f;
        b = a + "=";
        d = document.cookie.split(";");
        f = 0;
        while (f < d.length) {
          c = d[f];
          while (c.charAt(0) === " ") {
            c = c.substring(1, c.length);
          }
          if (c.indexOf(b) === 0) {
            return unescape(c.substring(b.length, c.length));
          }
          f++;
        }
        return null;
      }
    };
    _bookie.track();
    g = 0;
    j = h.length;
    results = [];
    while (g < j) {
      _bookie.push(h[g]);
      results.push(g++);
    }
    return results;
  })();

}).call(this);
