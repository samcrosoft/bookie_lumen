<?php

namespace App\Http\Controllers\Ping;

use App\Custom\Repository\Stats\StatsRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


/**
 * Created by PhpStorm.
 * User: Samuel
 * Date: 15/04/2015
 * Time: 14:16
 */

/**
 * Class PingController
 *
 * @package App\Http\Controllers\Ping
 */
class PingController extends Controller
{


    /**
     * This will be used to save the stats
     *
     * @param StatsRepository $oStatsRepository
     * @param Request         $oRequest
     *
     * @return Response
     */
    public function getIndex(StatsRepository $oStatsRepository, Request $oRequest)
    {

        $oStatsRepository->resolveStatsFromRequest($oRequest);

        return response(get_image())
            ->header('Content-Type', 'image/gif')
            ->header('Expires', 'Wed, 11 Nov 1998 11:11:11 GMT')
            ->header('Cache-Control', 'no-cache');
        //->header("Cache-Control",   "must-revalidate");
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getTest()
    {
        return view('ping.sample.index');
    }

}