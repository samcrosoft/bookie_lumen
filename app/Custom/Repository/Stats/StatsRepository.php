<?php
/**
 * Created by PhpStorm.
 * User: Samuel
 * Date: 15/04/2015
 * Time: 16:00
 */

namespace App\Custom\Repository\Stats;

use App\Jobs\Pings\SavePingTrackToDatabaseJob;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\DispatchesJobs;


/**
 * Class StatsRepository
 *
 * @package App\Custom\Repository\Stats
 */
class StatsRepository
{

    use DispatchesJobs;

    /**
     * @param Request $request
     */
    public function resolveStatsFromRequest(Request $request)
    {
        /*
         * Dispatch the job
         */
        $this->serveTrackingDataAsAJob($request);

    }


    /**
     * @param Request $request
     */
    protected function serveTrackingDataAsAJob(Request $request)
    {
        $oJob = new SavePingTrackToDatabaseJob($request);
        $this->dispatch($oJob);
    }


}