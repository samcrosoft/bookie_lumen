var elixir = require('laravel-elixir');

/*
 |----------------------------------------------------------------
 | Have a Drink
 |----------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic
 | Gulp tasks for your Laravel application. Elixir supports
 | several common CSS, JavaScript and even testing tools!
 |
 */


/**
 * build the bookie
 */
elixir(function (mix) {
    mix.coffee(["bookie.coffee"], "public/bookie.js");
    //.phpUnit();
});


/**
 * build the bookie call
 */
elixir(function (mix) {
    mix.coffee(['bookie-call.coffee'], "public/bookie-call.js");
});