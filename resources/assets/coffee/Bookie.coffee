_bookie = _bookie or []
(->
  h = (if (_bookie["slice"]) then _bookie.slice(0) else [])
  _bookie =
    track_referrer: true
    image: new Image()
    track: ->
      @setCookie "_bookie_cookie", 1, 1
      a = @url()
      if a
        @image.src = a
        b = 60 * 60
        d = b * 24
        f = d * 31
        c = d * 365
        i = c * 10
        @setCookie "_bookie_unique_hour", 1, b  unless @getCookie("_bookie_unique_hour")
        @setCookie "_bookie_unique_day", 1, d  unless @getCookie("_bookie_unique_day")
        @setCookie "_bookie_unique_month", 1, f  unless @getCookie("_bookie_unique_month")
        @setCookie "_bookie_unique_year", 1, c  unless @getCookie("_bookie_unique_year")
        @setCookie "_bookie_unique", 1, i  unless @getCookie("_bookie_unique")

    push: (a) ->
      b = a.shift()
      _bookie.track()  if b is "track"

    url: ->
      a = undefined
      b = undefined
      d = @$("bookie-tracker")
      if d
        b = d.getAttribute("data-bookie-id")
        #This will replace the track.js to the endpoint for the track
        a = d.src.replace("/bookie.js", "/track.gif")
        #        a = d.src.replace("/bookie.js", "/track")
        #        a = d.src.replace("/bookie-call.js", "/track.gif")
        a += "?h[site_id]=" + b
        a += "&h[resource]=" + @resource()
        a += "&h[referrer]=" + @referrer()
        a += "&h[title]=" + @title()
        a += "&h[user_agent]=" + @agent()
        a += "&h[unique]=" + @unique()
        a += "&h[unique_hour]=" + @uniqueHour()
        a += "&h[unique_day]=" + @uniqueDay()
        a += "&h[unique_month]=" + @uniqueMonth()
        a += "&h[unique_year]=" + @uniqueYear()
        a += "&h[screenx]=" + @screenWidth()
        a += "&h[browserx]=" + @browserWidth()
        a += "&h[browsery]=" + @browserHeight()
        a += "&timestamp=" + @timestamp()
      a

    domain: ->
      window.location.hostname

    referrer: ->
      a = ""
      return a  unless @track_referrer
      @track_referrer = false
      try
        a = top.document.referrer
      catch e1
        try
          a = parent.document.referrer
        catch e2
          a = ""
      a = document.referrer  if a is ""
      @escape a

    agent: ->
      @escape navigator.userAgent

    escape: (a) ->
      (if (typeof (encodeURIComponent) is "function") then encodeURIComponent(a) else escape(a))

    resource: ->
      @escape document.location.href

    timestamp: ->
      new Date().getTime()

    title: ->
      (if (document.title and document.title isnt "") then @escape(document.title) else "")

    uniqueHour: ->
      return 0  unless @getCookie("_bookie_cookie")
      (if @getCookie("_bookie_unique_hour") then 0 else 1)

    uniqueDay: ->
      return 0  unless @getCookie("_bookie_cookie")
      (if @getCookie("_bookie_unique_day") then 0 else 1)

    uniqueMonth: ->
      return 0  unless @getCookie("_bookie_cookie")
      (if @getCookie("_bookie_unique_month") then 0 else 1)

    uniqueYear: ->
      return 0  unless @getCookie("_bookie_cookie")
      (if @getCookie("_bookie_unique_year") then 0 else 1)

    unique: ->
      return 0  unless @getCookie("_bookie_cookie")
      (if @getCookie("_bookie_unique") then 0 else 1)

    screenWidth: ->
      try
        return screen.width
      catch e
        return 0

    browserDimensions: ->
      a = 0
      b = 0
      try
        if typeof (window.innerWidth) is "number"
          a = window.innerWidth
          b = window.innerHeight
        else if document.documentElement and document.documentElement.clientWidth
          a = document.documentElement.clientWidth
          b = document.documentElement.clientHeight
        else if document.body and document.body.clientWidth
          a = document.body.clientWidth
          b = document.body.clientHeight
      width: a
      height: b

    browserWidth: ->
      @browserDimensions().width

    browserHeight: ->
      @browserDimensions().height

    $: (a) ->
      return document.getElementById(a)  if document.getElementById
      null

    setCookie: (a, b, d) ->
      f = undefined
      c = undefined
      b = escape(b)
      if d
        f = new Date()
        f.setTime f.getTime() + (d * 1000)
        c = "; expires=" + f.toGMTString()
      else
        c = ""
      document.cookie = a + "=" + b + c + "; path=/"

    getCookie: (a) ->
      b = a + "="
      d = document.cookie.split(";")
      f = 0

      while f < d.length
        c = d[f]
        c = c.substring(1, c.length)  while c.charAt(0) is " "
        return unescape(c.substring(b.length, c.length))  if c.indexOf(b) is 0
        f++
      null

  _bookie.track()
  g = 0
  j = h.length

  while g < j
    _bookie.push h[g]
    g++)()