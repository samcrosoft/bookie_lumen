<?php
/**
 * Created by PhpStorm.
 * User: Adebola
 * Date: 12/29/2015
 * Time: 11:46 PM
 */

namespace App\Custom\Names;

/**
 * Class TableNames
 * @package App\Custom\Names
 */
class TableNames
{

    /**
     * @const string
     */
    const PING_TRACK_TABLE = "ping_track";
}