<?php
/**
 * Created by PhpStorm.
 * User: Adebola
 * Date: 01/09/2015
 * Time: 12:27
 */

namespace App\Custom\Repository\Store;

/**
 * Class BookieStore
 * @package App\Custom\Repository\Store
 */
abstract class BookieStore
{
    /**
     * perform the insert
     * @param array $aPingData
     * @return boolean
     */
    abstract public function insert($aPingData = []);

    /**
     * perform the deletion of ping
     * @return mixed
     */
    abstract public function remove();
}