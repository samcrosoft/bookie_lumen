<?php
/**
 * Created by PhpStorm.
 * User: Samuel
 * Date: 15/04/2015
 * Time: 14:32
 */


/*
 * Image Related Constants
 */

const IMAGE_TYPE_FILE = 'file';
const IMAGE_TYPE_FOLDER = 'folder';
const IMAGE_TYPE_HIDDEN_FILE = 'hidden_file';
const IMAGE_TYPE_LINK = 'link';
const IMAGE_TYPE_SMILEY = 'smiley';
const IMAGE_TYPE_ARROW = 'arrow';
const IMAGE_TYPE_BLANK = 'blank';