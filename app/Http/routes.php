<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
 * Ping Related Roues
 */

use App\Custom\Names\RouteNames;

$app->get('/', function () use ($app) {
    return "welcome home here";
    return $app->welcome();
});


/**
 * Random app key
 */
$app->get('/key', function() {
    return str_random(32);
});


/*
 * -----------------------------------
 * Ping Related Routes
 * -----------------------------------
 */
$app->group(['namespace' => 'App\Http\Controllers\Ping'], function () use ($app) {

    $app->get('track.gif', ['as' => RouteNames::ROUTE_PING, 'uses' => 'PingController@getIndex']);
    $app->get('track/test', ['as' => RouteNames::ROUTE_PING_TEST, 'uses' => 'PingController@getTest']);

});
