<?php
/**
 * Created by PhpStorm.
 * User: Samuel
 * Date: 15/04/2015
 * Time: 15:48
 */

namespace App\Custom\Names;

/**
 * Class RouteNames
 * @package App\Custom\Names
 */
class RouteNames {

    const ROUTE_PING = "ping";
    const ROUTE_PING_TEST = "ping.test";

}