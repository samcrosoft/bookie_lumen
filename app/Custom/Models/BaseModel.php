<?php
/**
 * Created by PhpStorm.
 * User: Adebola
 * Date: 17/11/2016
 * Time: 19:03
 */

namespace App\Custom\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @package App\Custom\Models
 */
class BaseModel extends Model
{
    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }
}