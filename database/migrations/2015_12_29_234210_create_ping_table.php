<?php

use App\Custom\Models\PingTrack;
use App\Custom\Names\TableNames;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create(TableNames::PING_TRACK_TABLE, function (Blueprint $table) {
            $oModel = new PingTrack();
            $table->bigIncrements($oModel->getKeyName());


            $table->bigInteger("site_id");
            $table->string("resource", 500);
            $table->string("referrer", 500);
            $table->string("title", 500);
            $table->string("user_agent", 500);
            $table->unsignedInteger("unique");
            $table->unsignedInteger("unique_hour");
            $table->unsignedInteger("unique_day");
            $table->unsignedInteger("unique_month");
            $table->unsignedInteger("screenx");
            $table->unsignedInteger("browserx");
            $table->unsignedInteger("browsery");
            $table->unsignedInteger("timestamp");


//            "site_id" => "348674836483463463"
//    "resource" => "http://bookie.dev/track/test"
//    "referrer" => ""
//    "title" => "Ping Test"
//    "user_agent" => "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36"
//    "unique" => "1"
//    "unique_hour" => "1"
//    "unique_day" => "1"
//    "unique_month" => "1"
//    "unique_year" => "1"
//    "screenx" => "1920"
//    "browserx" => "1920"
//    "browsery" => "730"

            /**
             * Enable the timestamps
             */
            if ($oModel->usesTimestamps())
                $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop(TableNames::PING_TRACK_TABLE);
    }
}
