(function() {
  var b, d;

  b = document.createElement("script");

  b.type = "text/javascript";

  b.async = true;

  b.id = "bookie-tracker";

  b.setAttribute('data-bookie-id', '348674836483463463');

  b.src = "/bookie.js";

  d = document.getElementsByTagName("script");

  d = d[0];

  d.parentNode.insertBefore(b, d);


  /*
  This is how it is called by google analytics
  
  <script type="text/javascript">
  var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
  document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  </script>
  <script type="text/javascript">
  try{
  var pageTracker = _gat._getTracker("UA-xxxxxx-x");
  pageTracker._trackPageview();
  } catch(err) {}</script>
   */

}).call(this);
