<?php
/**
 * Created by PhpStorm.
 * User: Adebola
 * Date: 01/09/2015
 * Time: 12:23
 */

namespace App\Providers\Store;


use App\Custom\Repository\Store\BookieStore;
use App\Custom\Repository\Store\MongoDB\BookieMongoDBStore;
use Illuminate\Support\ServiceProvider;

/**
 * Class BookieMongoStoreProvider
 * @package App\Providers\Store
 */
class BookieStoreProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     * @throws \Exception
     */
    public function register()
    {

        /*
         * Register the bookie store class to the mongodb store
         */
        $this->app->bind(BookieStore::class, function($app){
            return new BookieMongoDBStore();
        });
    }
}