<?php
/**
 * Created by PhpStorm.
 * User: Samuel
 * Date: 15/04/2015
 * Time: 14:21
 */


if (!function_exists("get_image")) {

    /**
     * @param string $sImage
     * @return null|string
     */
    function get_image($sImage = IMAGE_TYPE_BLANK)
    {
        $sReturn = null;

        switch ($sImage) {
            case IMAGE_TYPE_FILE:
                $sReturn = base64_decode('R0lGODlhEQANAJEDAJmZmf///wAAAP///yH5BAHoAwMALAAAA
AARAA0AAAItnIGJxg0B42rsiSvCA/REmXQWhmnih3LUSGaqg35vF
bSXucbSabunjnMohq8CADsA');
                break;
            case IMAGE_TYPE_FOLDER:
                $sReturn = base64_decode('R0lGODlhEQANAJEDAJmZmf///8zMzP///yH5BAHoAwMALAAAAA
ARAA0AAAIqnI+ZwKwbYgTPtIudlbwLOgCBQJYmCYrn+m3smY5v
Gc+0a7dhjh7ZbygAADsA');
                break;
            case IMAGE_TYPE_HIDDEN_FILE:
                $sReturn = base64_decode('R0lGODlhEQANAJEDAMwAAP///5mZmf///yH5BAHoAwMALAAAA
AARAA0AAAItnIGJxg0B42rsiSvCA/REmXQWhmnih3LUSGaqg35vF
bSXucbSabunjnMohq8CADsA');
                break;
            case IMAGE_TYPE_LINK:
                $sReturn = base64_decode('R0lGODlhEQANAKIEAJmZmf///wAAAMwAAP///wAAAAAAAAAAA
CH5BAHoAwQALAAAAAARAA0AAAM5SArcrDCCQOuLcIotwgTYUll
NOA0DxXkmhY4shM5zsMUKTY8gNgUvW6cnAaZgxMyIM2zBLCaHlJgAADsA');
                break;
            case IMAGE_TYPE_SMILEY:
                $sReturn = base64_decode('R0lGODlhEQANAJECAAAAAP//AP///wAAACH5BAHoAwIALAAAA
AARAA0AAAIslI+pAu2wDAiz0jWD3hqmBzZf1VCleJQch0rkdnppB3
dKZuIygrMRE/oJDwUAOwA=');
                break;
            case IMAGE_TYPE_ARROW:
                $sReturn = base64_decode('R0lGODlhEQANAIABAAAAAP///yH5BAEKAAEALAAAAAARAA0AA
AIdjA9wy6gNQ4pwUmav0yvn+hhJiI3mCJ6otrIkxxQAOw==');
                break;
            case IMAGE_TYPE_BLANK:
                // using base decode
                $sReturn = base64_decode("R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7");
                break;
        }

        return $sReturn;
    }

}