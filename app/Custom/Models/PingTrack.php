<?php
/**
 * Created by PhpStorm.
 * User: Adebola
 * Date: 12/29/2015
 * Time: 11:49 PM
 */

namespace App\Custom\Models;


use App\Custom\Names\TableNames;

/**
 * Class PingTrack
 * @package App\Custom\Models
 */
class PingTrack extends BaseModel
{

    /**
     * make all attributes mass assignable
     *
     * @var array
     */
    var $guarded = [];

    /**
     * @var string
     */
    protected $table = TableNames::PING_TRACK_TABLE;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $primaryKey = "ping_id";

}