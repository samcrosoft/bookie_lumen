<?php
/**
 * Created by PhpStorm.
 * User: Adebola
 * Date: 01/09/2015
 * Time: 12:26
 */

namespace App\Custom\Repository\Store\MongoDB;


use App\Custom\Repository\Store\BookieStore;

use MongoDB\Client;
use MongoDB\Collection;
use MongoDB\Database;

/**
 * Class BookieMongoDBStore
 * @package App\Custom\Repository\Store\MongoDB
 */
class BookieMongoDBStore extends BookieStore
{

    /**
     * @var null|Client
     */
    private $oConnection = null;

    /**
     * @var Database
     */
    protected $oMongoDatabase;

    /**
     * @var Collection|null
     */
    protected $oMongoCollection;
    /**
     * @const string
     */
    const DEFAULT_DATABASE_NAME = 'bookie';
    /**
     * @const string
     */
    const DEFAULT_COLLECTION_NAME = 'pings';

    /**
     * @throws \Exception,\LogicException
     */
    public function __construct()
    {
        $sDSN = env('BOOKIE_MONGO_DB_DSN');
        $sDatabaseName = env('BOOKIE_MONGO_DB');
        $sCollectionName = env('BOOKIE_MONGO_COLLECTION');
        if (empty($sDSN)) {
            throw new \LogicException('Bookie Mongo DSN Cannot Be Empty');
        }
        if (empty($sDatabaseName) || empty($sCollectionName)) {
            throw new \LogicException('Either The Database Name Or The Collection name Cannot Be Empty');
        }

        if ($this->isMongoDBEnabled()) {
            try {
                $this->oConnection = new Client($sDSN);
                $this->oMongoDatabase = $this->getMongoConnection()->selectDatabase($sDatabaseName);
                $this->oMongoCollection = $this->getMongoDatabase()->selectCollection($sCollectionName);
            } catch (\MongoConnectionException $e) {
                throw new \Exception('Please Validate The MongoDB Connection Is Up And Running!');
            }
        }
    }

    /**
     * This method will return a boolean that indicates if the mongodb extension is loaded
     * @return bool
     */
    private function isMongoDBEnabled(): bool
    {
        return extension_loaded('mongodb');
    }

    /**
     * @return Client|null
     */
    private function getMongoConnection(): ?Client
    {
        return $this->oConnection;
    }

    /**
     * @return Collection|mixed
     */
    protected function getMongoCollection()
    {
        return $this->oMongoCollection;
    }

    /**
     * @return Database
     */
    protected function getMongoDatabase(): Database
    {
        return $this->oMongoDatabase;
    }

    /**
     * @return bool
     */
    protected function isConnected(): bool
    {
        return null !== $this->getMongoConnection() && $this->getMongoConnection();
    }


    /**
     * @inheritdoc
     */
    public function insert($aPingData = []): bool
    {
        $bReturn = false;
        if ($this->isConnected()) {
            $oResult = $this->getMongoCollection()->insertOne($aPingData);
            $bReturn = !empty($oResult);
        }
        return $bReturn;
    }

    /**
     * @inheritdoc
     */
    public function remove()
    {
        // TODO: Implement remove() method.
    }
}