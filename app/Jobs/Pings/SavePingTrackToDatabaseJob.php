<?php
/**
 * Created by PhpStorm.
 * User: Adebola
 * Date: 01/09/2015
 * Time: 12:14
 */

namespace App\Jobs\Pings;

use App\Custom\Repository\Store\BookieStore;
use App\Jobs\Job;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class SavePingTrackToDatabaseJob
 *
 * @package App\Jobs\Pings
 */
class SavePingTrackToDatabaseJob extends Job
{
    const TIMESTAMP_KEY = 'timestamp';
    /**
     * @const string
     */
    const PING_DATA_SITE_ID = 'site_id';

    /**
     * @const int
     */
    const MAX_NUMBER_OF_FAILURE_ATTEMPTS = 3;

    /**
     * @var string
     */
    private $ipAddress;

    /**
     * @var null|array
     */
    private $aPingData;

    /**
     * @param Request $request
     *
     */
    public function __construct(Request $request)
    {
        $aTrackingDetails = $request->all();
        $sTrackingTimestamp = $request->get(self::TIMESTAMP_KEY);


        /*
         * set the timestamp
         */
        if (null !== $sTrackingTimestamp) {
            $aTrackingDetails[ self::TIMESTAMP_KEY ] = $sTrackingTimestamp;
        }

        $this->ipAddress = $request->ip();

        $this->aPingData = $aTrackingDetails;
    }


    /**
     * @param string $ipAddress
     *
     * @return array
     * @internal param Request $request
     *
     */
    protected function detectGeoDataForRequest(string $ipAddress): array
    {
        /*
        $geoHttpEndpoint = env('BOOKIE_GEO_ENDPOINT');
        if (null !== $geoHttpEndpoint && !empty($ipAddress)) {
            try {
                $res = Zttp::get($geoHttpEndpoint, [
                    'query' => ['ip' => $ipAddress],
                ]);
                $status = (int)$res->getStatusCode();
                if ($status === Response::HTTP_OK) {
                    $geoData = $res->json();
                }
            } catch (\Exception $exception) {
                // do nothing
            }
        }
        */
        return $this->getGeoDataFromFreeGeoIp($ipAddress);
    }


    /**
     * @param string $ipAddress
     *
     * @return array
     */
    private function getGeoDataFromFreeGeoIp(string $ipAddress): array
    {
        $ipAddress = strtolower($ipAddress);
        $ipAddress = in_array($ipAddress, ['localhost', '127.0.0.1'], true) ? '' : $ipAddress;
        $geoHttpEndpoint = "http://freegeoip.net/json/$ipAddress";
        $geoData = [];
        $client = new Client();

        if (null !== $geoHttpEndpoint) {
            try {
                $res = $client->get($geoHttpEndpoint);
                $status = (int)$res->getStatusCode();

                if ($status === Response::HTTP_OK) {
                    $rateLimitingHeader = $res->getHeader('X-Ratelimit-Remaining');
                    // retrieve the rate limit
                    $remainingLimit = is_array($rateLimitingHeader) ? head($rateLimitingHeader) : 0;
                    if ($remainingLimit === 0) {
                        throw new \LogicException('You Have Exceeded Your Rate Limit');
                    }

                    $json = json_decode($res->getBody(), true);
                    $latitude = array_get($json, 'latitude');
                    $longitude = array_get($json, 'longitude');
                    if ($latitude !== null & $longitude !== null) {
                        $geoData = compact('latitude', 'longitude');
                    }
                }
            } catch (\Exception $exception) {
                // do nothing
                dump($exception->getMessage());
            }
        }
        return $geoData;
    }

    /**
     * Execute the job.
     *
     * @param BookieStore $oBookieStore
     */
    public function handle(BookieStore $oBookieStore)
    {

        if (
            $this->attempts() > self::MAX_NUMBER_OF_FAILURE_ATTEMPTS
        ) {
            // delete the job if it has failed multiple times
            $this->delete();
        }

        /*
         * Add geo reference data
         */
        $aTrackingDetails = $this->aPingData;
        $geoData = $this->detectGeoDataForRequest($this->ipAddress);
        if (!empty($geoData)) {
            $aTrackingDetails['geo'] = $geoData;
        }

        $oBookieStore->insert($aTrackingDetails);
    }
}